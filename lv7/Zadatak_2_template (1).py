import numpy as np
import matplotlib.pyplot as plt
import matplotlib.image as Image
from sklearn.cluster import KMeans

# ucitaj sliku
img = Image.imread("imgs\\test_1.jpg")

# prikazi originalnu sliku
plt.figure()
plt.title("Originalna slika")
plt.imshow(img)
plt.tight_layout()
plt.show()

# pretvori vrijednosti elemenata slike u raspon 0 do 1
img = img.astype(np.float64) / 255

# transfromiraj sliku u 2D numpy polje (jedan red su RGB komponente elementa slike)
w,h,d = img.shape
img_array = np.reshape(img, (w*h, d))

# rezultatna slika
img_array_aprox = img_array.copy()

# Zad 1
unique_colors = np.unique(img_array_aprox, axis=0)
print("Broj razlicitih boja:", len(unique_colors))

# Zad 2, 3, 4
km = KMeans(n_clusters=5)
km.fit(img_array)

centroids=km.cluster_centers_
labels=km.predict(img_array)

img_array_approx = centroids[labels]
img_approx = np.reshape(img_array_approx, (w, h, d))

plt.figure()
plt.title("Kopija slike")
plt.imshow(img_approx)
plt.tight_layout()
plt.show()
