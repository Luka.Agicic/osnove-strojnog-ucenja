import pandas as pd
import matplotlib.pyplot as plt
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import MinMaxScaler
from sklearn.linear_model import LinearRegression
from sklearn.metrics import mean_absolute_error, mean_absolute_percentage_error, mean_squared_error, r2_score

# Zadatak pod a)
data = pd.read_csv('data_C02_emission.csv')

data = data.drop(["Make","Model"], axis=1)

input_variables=['Fuel Consumption City (L/100km)',
                 'Fuel Consumption Hwy (L/100km)',
                 'Fuel Consumption Comb (L/100km)',
                 'Fuel Consumption Comb (mpg)',
                 'Engine Size (L)',
                 'Cylinders']

output_variable = ['CO2 Emissions (g/km)']
X = data[input_variables]
y = data[output_variable]

X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=1)

# Zadatak pod b)
plt.figure()
plt.scatter(X_train['Fuel Consumption Comb (L/100km)'], y_train, c="red")
plt.scatter(X_test['Fuel Consumption Comb (L/100km)'], y_test, c="green")
plt.show()

# Zadatak pod c)
X_test['Fuel Consumption Comb (L/100km)'].plot(kind='hist', bins=20)
plt.show()

sc = MinMaxScaler()

X_train_n = sc.fit_transform(X_train)
plt.hist(X_train_n[:, 0],bins=20)
plt.show()

X_test_n = sc.transform(X_test)

# Zadatak pod d)
linearModel = LinearRegression()
linearModel.fit(X_train_n, y_train)
print(linearModel.coef_)

# Zadatak pod e)
y_test_p = linearModel.predict(X_test_n)
plt.figure()
plt.scatter(X_test_n[:,2], y_test, c="green")
plt.scatter(X_test_n[:,2], y_test_p, c="red")
plt.show()

# Zadatak pod f)

MAE=mean_absolute_error(y_test, y_test_p)
MSE=mean_squared_error(y_test, y_test_p)
RMSE=mean_squared_error(y_test, y_test_p, squared=False)
MAPE=mean_absolute_percentage_error(y_test, y_test_p)

R_SQUARED=r2_score(y_test, y_test_p)

print(f"MAE: {MAE}, MSE: {MSE}, RMSE: {RMSE}, MAPE: {MAPE}, R-Squared: {R_SQUARED}")

