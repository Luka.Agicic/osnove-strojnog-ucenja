import numpy as np
import matplotlib.pyplot as plt
img = plt.imread("road.jpg")
plt.figure()
plt.imshow(img, cmap="gist_gray", alpha=0.7)
plt.show()
