import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.colors import ListedColormap

from sklearn.linear_model import LogisticRegression
from sklearn.neighbors import KNeighborsClassifier
from sklearn import svm
from sklearn.svm import SVC

from sklearn.metrics import accuracy_score
from sklearn.model_selection import KFold, train_test_split
from sklearn.preprocessing import StandardScaler
from sklearn.pipeline import Pipeline
from sklearn.pipeline import make_pipeline
from sklearn.model_selection import GridSearchCV
from sklearn . svm import SVC
from sklearn . preprocessing import StandardScaler
from sklearn . pipeline import Pipeline
from sklearn . pipeline import make_pipeline
from sklearn . model_selection import GridSearchCV
from sklearn . model_selection import cross_val_score

data = pd.read_csv("Social_Network_Ads.csv")

X = data[["Age","EstimatedSalary"]].to_numpy()
y = data["Purchased"].to_numpy()

X_train, X_test, y_train, y_test = train_test_split(X, y, test_size = 0.2, stratify=y, random_state = 10)

param_grid = {'n_neighbors': [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 15, 20, 30, 40, 50, 100]}
KNN_model = KNeighborsClassifier(n_neighbors = 1)
grid = GridSearchCV(KNN_model, param_grid , cv =5, scoring ='accuracy', n_jobs =-1)

sc = StandardScaler()
X_train_n = sc.fit_transform(X_train)
grid.fit(X_train_n, y_train)

print ( grid . best_params_ )
print ( grid . best_score_ )
print ( grid . cv_results_ )

