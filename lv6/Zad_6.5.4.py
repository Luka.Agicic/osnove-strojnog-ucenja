import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.colors import ListedColormap

from sklearn.linear_model import LogisticRegression
from sklearn.neighbors import KNeighborsClassifier
from sklearn import svm

from sklearn.metrics import accuracy_score
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler
from sklearn.pipeline import Pipeline
from sklearn.pipeline import make_pipeline
from sklearn.model_selection import GridSearchCV

data = pd.read_csv("Social_Network_Ads.csv")

X = data[["Age","EstimatedSalary"]].to_numpy()
y = data["Purchased"].to_numpy()

X_train, X_test, y_train, y_test = train_test_split(X, y, test_size = 0.2, stratify=y, random_state = 10)

sc = StandardScaler()
X_train_n = sc.fit_transform(X_train)

param_grid = {'C':[0.01, 0.1, 1, 10, 100],
    'gamma':[1, 0.1, 0.01, 0.001, 0.0001],
    }

SVM_model = svm.SVC(kernel='rbf')

svm_gscv = GridSearchCV (SVM_model, param_grid , cv = 5 , scoring ='accuracy', n_jobs = -1)
svm_gscv.fit( X_train, y_train)
print(svm_gscv.best_params_)