try:
    number = float(input())

    if number < 0.0 or number > 1.0:
        print("You entered number that is out of scope")
    elif number >= 0.9 and number <= 1.0:
        print('A')
    elif number >= 0.8 and number < 0.9:
        print('B')
    elif number >= 0.7 and number < 0.8:
        print('C')
    elif number >= 0.6 and number < 0.7:
        print('D')
    elif number < 0.6:
        print('F')

except ValueError:
    print('Pogresan unos')

