import numpy as np
from tensorflow import keras
from tensorflow.keras import layers
from matplotlib import pyplot as plt
from sklearn.metrics import confusion_matrix
from keras.models import load_model

model = load_model("z1.model.keras")
(x_train, y_train), (x_test, y_test) = keras.datasets.mnist.load_data()

x_train_s = x_train.astype("float32")/255
x_test_s = x_test.astype("float32")/255

x_train_s = np.expand_dims(x_train_s, -1)
x_test_s = np.expand_dims(x_test_s, -1)

predictions = model.predict(x_test_s)
prediction_labels = np.argmax(predictions, axis=1)
wrong_labels = np.where(prediction_labels != y_test)[0]

for i in range(9):
    index = wrong_labels[i]
    plt.subplot(3,3,i+1)
    plt.imshow(x_test[index].reshape(28,28))
    plt.title(f"IS: {y_test[index]}, PREDICTED: {prediction_labels[index]}")
    plt.axis("off")
plt.show()
