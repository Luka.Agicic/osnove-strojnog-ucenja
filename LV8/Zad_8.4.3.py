import numpy as np
from tensorflow import keras
from tensorflow.keras import layers
from matplotlib import pyplot as plt
from sklearn.metrics import confusion_matrix
from keras.models import load_model
from PIL import Image

model = keras.models.load_model("z1.model.keras")

numbers = ["zero", "one", "two", "three", "four", "five", "six", "seven", "eight", "nine"]

def predict(path):
    image = Image.open(path).convert("L").resize((28,28))
    image_array = np.array(image).astype("float32")/255
    image_array = np.expand_dims(image_array, axis=0)
    image_array = np.expand_dims(image_array, axis=-1)

    predicted_label = np.argmax(model.predict(image_array))
    print(predicted_label)

for i in range(10):
    print(f"Testiranje broja {numbers[i]}")
    predict(f"nums/{numbers[i]}.png")
    print()
