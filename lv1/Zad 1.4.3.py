def inputNumbers():
    numbers = []
    while True:
        number = input("Unesi broj: ")
    
        if number == 'Done':
            break
        try:
            number = float(number)
            numbers.append(number)
        except:
            print("Pogresan unos")
    return numbers

numbersList = inputNumbers()

print("Uneseno je:", len(numbersList))

average = sum(numbersList)/len(numbersList)

print('Srednja: ', average)
print('Minimalno: ', min(numbersList))
print('Minimalno: ', max(numbersList))
print('Sortirano: ', sorted(numbersList))