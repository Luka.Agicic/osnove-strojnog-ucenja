import numpy as np
import matplotlib.pyplot as plt

arr=np.loadtxt("data.csv",delimiter=",",dtype=str)
arr=arr[1::]
arr=np.array(arr, np.float64)
print(f"Broj izmjerenih ljudi: {len(arr)}")
x=arr[:,1]
y=arr[:,2]
plt.scatter(x,y)
plt.show()

tempX=arr[0::50, 1]
tempY=arr[0::50, 2]
plt.scatter(tempX,tempY)
plt.show()

print(f"Prosjecna visina: {round(tempX.mean(),2)}, minimalna visina: {round(tempX.min(), 2)}, maksimalna visina: {round(tempX.max(), 2)}")

indMan = arr[arr[:,0] == 1]
indWoman = arr[arr[:,0] == 0]

print(f"Prosjecna visina za muskarca: {indMan[:,1].mean()}, prosjecna visina za zenu: {indWoman[:,1].mean()}")
print(f"Minimalna visina za muskarca: {indMan[:,1].min()}, minimalna visina za zenu: {indWoman[:,1].min()}")
print(f"Maksimalna visina za muskarca: {indMan[:,1].max()}, maksimalna visina za zenu: {indWoman[:,1].max()}")
