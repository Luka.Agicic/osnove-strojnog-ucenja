import pandas as pd
import numpy as np

data = pd.read_csv('data_C02_emission.csv')

# Zadatak pod a)
print(len(data))
print(data.dtypes)
print(data.drop_duplicates())

for column in ['Transmission','Fuel Type']:
    data[column] = data[column].astype('category')

print(data.dtypes)

# Zadatak pod b)
sortedData = data.sort_values(by=['Fuel Consumption City (L/100km)'], ascending=True)[['Make','Model','Fuel Consumption City (L/100km)']]
sortedDataDesc = data.sort_values(by=['Fuel Consumption City (L/100km)'], ascending=False)[['Make','Model','Fuel Consumption City (L/100km)']]
print(sortedData.head(3))
print(sortedDataDesc.head(3))

# Zadatak pod c)
selectedData=data[(data['Engine Size (L)'] >= 2.5) & (data['Engine Size (L)'] <= 3.5)]
length=len(selectedData['Make'])
print(f"Number of vehicles with engines between 2.5 and 3.5L: {length}")
avg = data['CO2 Emissions (g/km)'].mean()
print(f"Average C02 emission: {avg}")

# Zadatak pod d)
certainCarName = data[data['Make'] == 'Audi']
certainCarLength = len(certainCarName['Make'])
print(f"There are {certainCarLength} Audi's")

numberOfCylinders = certainCarName[certainCarName['Cylinders'] == 4]
print(f"Mean C02 emission is: {numberOfCylinders['CO2 Emissions (g/km)'].mean().round()}")

# Zadatak pod e)
sortedByCylinders=data['Cylinders'].value_counts().sort_index()
print(sortedByCylinders)

emissionPerCylinder=data.groupby('Cylinders')['CO2 Emissions (g/km)'].mean()
print(emissionPerCylinder)

# Zadatak pod g)
diesels = data[(data['Fuel Type'] == 'D')]
petrols = data[(data['Fuel Type'] == 'Z')]

fourCylinderDiesels=diesels

# Zadatak pod h)
manuals=data[(data['Transmission'].str[0] == 'M')]
length=len(manuals['Make'])
print(f"There are {length} manuals")

# Zadatak pod i)

print(data.corr(numeric_only=True))
